#!/bin/bash

if [[ $1 = "--help" ]]; then
echo "init    - Running init process"
echo "dev     - Running Django dev server"
echo "migrate - makemigratea; migrate; schema"
echo "schema  - graphql_schema --indent 2"
echo "flower  - celery -A app flower"
echo ""
echo "process - start main processing"
echo "processprod"
echo ""
echo "cbeat"
echo "cback"
echo "cpurge"
echo "cinspect"
echo ""
echo "celery"
echo "celery100"
echo "celeryprod"
exit 1
fi

if [[ $1 = "init" ]]; then
  echo "Running init process"
  ./run.sh migrate
  # Create users defined in settings
  python manage.py createusers

  rm /root/.ssh/known_hosts
  ssh-copy-id tamarin_main
  ssh-copy-id geobase_main

  exit 0
fi

if [[ $1 = "dev" ]]; then
  echo "Running Django develop web-server"
  pkill -f runserver
  python manage.py runserver 0.0.0.0:8000 &
  exit 0
fi

if [[ $1 = "migrate" ]]; then
  python manage.py makemigrations users
  python manage.py makemigrations dispetcher
  python manage.py migrate

  python manage.py graphql_schema --indent 2
  mv schema.json /data/.

  exit 1
fi

if [[ $1 = "schema" ]]; then
  python manage.py graphql_schema --indent 2
  mv schema.json /data/.
  exit 1
fi

if [[ $1 = "flower" ]]; then
  celery -A app flower
  exit 1
fi

################################################################

if [[ $1 = "doingest" ]]; then
  echo "Учёт новых файлов и постановка в очередь на обработку"
  ./exec.py doingest && ./run.sh celery
  exit 0
fi

if [[ $1 = "process1" ]]; then
  echo "Обрабока новых файлов"
  ./exec.py process && ./run.sh celeryinfo1
  exit 0
fi

if [[ $1 = "process" ]]; then
  echo "Обрабока новых файлов"
  ./exec.py process && ./run.sh celeryinfo10
  exit 0
fi

if [[ $1 = "processprod" ]]; then
  echo "Обрабока новых файлов"
  ./exec.py process && ./run.sh celeryprod
  exit 0
fi

if [[ $1 = "processprod1000" ]]; then
  echo "Обрабока новых файлов"
  ./exec.py process && ./run.sh celeryprod1000
  exit 0
fi

if [[ $1 = "processprod10" ]]; then
  echo "Обрабока новых файлов"
  ./exec.py process && ./run.sh celeryprod10
  exit 0
fi

################################################################
################################################################

if [[ $1 = "cbeat" ]]; then
  celery -A app beat
  exit 0
fi

if [[ $1 = "cback" ]]; then
  celery -A app worker --loglevel=info -Q back -n back

  exit 0
fi

if [[ $1 = "cinspect" ]]; then
  celery -A app inspect active

  exit 0
fi

if [[ $1 = "cpurge" ]]; then
  celery -A app purge -f
  celery -A app purge -Q back -f

  exit 0
fi

################################################################
# ./run.sh celery prod 10
if [[ $1 = "celerydebug1" ]]; then
  celery -A app worker --loglevel=debug --concurrency=1
  exit 0
fi

if [[ $1 = "celerydebug3" ]]; then
  celery -A app worker --loglevel=debug --concurrency=3
  exit 0
fi

if [[ $1 = "celerydebug10" ]]; then
  celery -A app worker --loglevel=debug --concurrency=10
  exit 0
fi

if [[ $1 = "celeryinfo1" ]]; then
  celery -A app worker --loglevel=info --concurrency=1
  exit 0
fi

if [[ $1 = "celeryinfo3" ]]; then
  celery -A app worker --loglevel=info --concurrency=3
  exit 0
fi

if [[ $1 = "celeryinfo5" ]]; then
  celery -A app worker --loglevel=info --concurrency=5
  exit 0
fi

if [[ $1 = "celeryinfo10" ]]; then
  celery -A app worker --loglevel=info --concurrency=10
  exit 0
fi

if [[ $1 = "celeryinfo100" ]]; then
  celery -A app worker --loglevel=info --concurrency=100
  exit 0
fi

if [[ $1 = "celeryinfo200" ]]; then
  celery -A app worker --loglevel=info --concurrency=200
  exit 0
fi

if [[ $1 = "celeryprod10" ]]; then
  celery -A app worker --concurrency=10
  exit 0
fi

if [[ $1 = "celeryprod100" ]]; then
  celery -A app worker --concurrency=100
  exit 0
fi

if [[ $1 = "celeryprod1000" ]]; then
  celery -A app worker --concurrency=1000
  exit 0
fi

################################################################
################################################################
################################################################
if [[ $1 = "prod" ]]; then
  echo "Running production Uwsgi web-server"
  pkill -f runserver
  uwsgi uwsgi.ini
  exit 0
fi

if [[ $1 = "jupyter" ]]; then
  jupyter notebook --ip 0.0.0.0 --allow-root
  exit 0
fi

if [[ $1 = "doexec" ]]; then
  ./exec.py doingest && ./exec.py doprocess
  exit 0
fi


# DEFAULT COMMANDS WITHOUT ARGUMENTS
./run.sh --help
