#!/bin/bash

if [[ $1 = "--help" ]]; then
  echo "Ключи для запуска:"
  echo "init - первичная инициализация системы"
  echo "start - запустить систему со всеми контейнерами"
  echo "rebuild - пересобрать и запустить все контейнеры системы"
  echo "archivetle - создать архив с TLE-данными"
  echo "stop - остановить комплекс со всеми контейнерами"

  exit 0
fi

if [[ $1 = "init" ]]; then
  clear
  # Create folder structure
  mkdir -p ./storage/data
  # echo "Создаём каталог storage для соответствия рабочему контуру"
  # sudo mkdir -p /srv/glustervol1/vmtk/storage/data
  # ln -s /srv/glustervol1/vmkt/storage ./storage

  # Try to stop containers to be sure
  docker-compose stop
  # Start containers:
  docker-compose up -d
  # Initializing Geobase
  docker exec -it kaokci_geobase_main ./run.sh init
  docker exec -it kaokci_tamarin_main ./run.sh init

  exit 0
fi

if [[ $1 = "start" ]]; then
  clear
  # Launch containers:
  echo "Запускаем контейнерную группировку комплекса"
  docker-compose up -d
  # Wait 10 seconds to be sure all services have been launched
  sleep 10s
  # Launch starting scripts in containers
  echo "Запускаем сервисы внутри контейнеров"
  docker exec -d kaokci_tamarin_main ./run.sh start
  docker exec -d kaokci_geobase_main ./run.sh start
  docker exec -d kaokci_dqs_main ./run.sh start
  docker exec -d kaokci_ccm_main ./run.sh flower
  docker exec -d kaokci_ccm_main ./run.sh cbeat
  docker exec -d kaokci_ccm_main ./run.sh dev
  # docker exec -d kaokci_ccm_main ./run.sh celeryprod10
  #
  exit 0
fi

if [[ $1 = "dev" ]]; then
  clear
  # Launch containers:
  echo "Запускаем контейнерную группировку комплекса"
  docker-compose up -d
  # Wait 3 seconds to be sure all services have been launched
  sleep 3s
  # Launch starting scripts in containers
  echo "Запускаем сервисы внутри контейнеров"
  docker exec -d kaokci_tamarin_main ./run.sh start
  docker exec -d kaokci_geobase_main ./run.sh start
  docker exec -d kaokci_dqs_main ./run.sh start
  docker exec -d kaokci_ccm_main ./run.sh flower
  # docker exec -d kaokci_ccm_main ./run.sh dev

  exit 0
fi

if [[ $1 = "archivetle" ]]; then
  # Create TLE archive till the current date #
  # Start containers to be sure
  docker-compose up -d
  # Run create archive script within container
  docker exec -d kaokci_geobase_main ./run.sh archivetle

  exit 0
fi

if [[ $1 = "rebuild" ]]; then
  clear
  echo "DO NOT run this command in parallel with other docker operations!!!"
  echo "clearing docker-cache..."
  echo "--------------------------------------------------------"
  docker rmi $(docker images -a --filter=dangling=true -q)
  echo "rebuilding..."
  docker-compose up -d --build
  # Start complex
  ./launcher.sh start

  exit 0
fi

if [[ $1 = "stop" ]]; then
  # Stop containers:
  docker-compose stop

  exit 0
fi

#################################################################
### ADDITIONAL SCRIPTS
#################################################################

# Запуск периодизатора задач Celery Beat
if [[ $1 = "cbeat" ]]; then
  docker exec -d kaokci_ccm_main ./run.sh cbeat
  exit 0
fi

# Запуск веб-сервера Django в режиме разработки
if [[ $1 = "djdev" ]]; then
  docker exec -d kaokci_ccm_main ./run.sh dev
  exit 0
fi

# Поиск новых файлов на обработку
if [[ $1 = "doingest" ]]; then
  docker exec -d kaokci_ccm_main ./exec.py doingest
  exit 0
fi

# Постановка на обработку новых файлов
if [[ $1 = "doprocess" ]]; then
  docker exec -d kaokci_ccm_main ./exec.py doprocess
  exit 0
fi

# Новая обрабока одной командой
if [[ $1 = "doexec" ]]; then
  docker exec -d kaokci_ccm_main ./exec.py doingest
  sleep 1s
  docker exec -d kaokci_ccm_main ./exec.py doprocess
  exit 0
fi

# Запуск тестирования
if [[ $1 = "test" ]]; then
  # Запуск комплекса в режиме разработки
  ./launcher.sh dev
  sleep 1s
  # echo "Выполняем инициализацию"
  # docker exec -d kaokci_ccm_main ./run.sh init
  echo "Запускаем веб-сервер"
  docker exec -d kaokci_ccm_main ./run.sh dev
  echo "Запускаем крон для повторяемой задачи doprocess"
  docker exec -d kaokci_ccm_main ./run.sh cbeat
  echo "Очищаем историю задач"
  docker exec -d kaokci_ccm_main ./manage.py tasksremoveallforce
  echo "Запускаем сэлери-пайплайн"
  docker exec -d kaokci_ccm_main ./run.sh celerytest5
  sleep 5s
  echo "Ставим в очередь все файлы из каталога ingest"
  docker exec -d kaokci_ccm_main ./exec.py doingest
  exit 0
fi

# Очистка всех задач
if [[ $1 = "tasksremoveall" ]]; then
  docker exec -it kaokci_ccm_main ./manage.py tasksremoveall
  exit 0
fi

# Remove files and directories from gitclean.txt file
if [[ $1 = "gitclean" ]]; then
  for i in `cat gitclean.txt`; do
    echo "Removing $i"
    rm -rf $i
  done
  git status

  exit 0
fi


# DEFAULT COMMANDS WITHOUT ARGUMENTS
./launcher.sh --help
